#set BUILD=profile as default
BUILD := profile
FRAMES := 10
FILE := foreman
CONFIG := config.h

#for report-making
TIME := $(shell date +'%y%m%d-%H-%M-%S')
HOST := $(shell hostname -s)
REPORT_DIR := raport_data

#flags
TIMER = 0

#common
cflags.common := -w  -march=native -DCONFIG_FILE=$(CONFIG) -O3
ldflags.common := -lm

#default
cflags.profile := $(cflags.common)
ldflags.profile := $(ldflags.common) 

#use "make BUILD=release" to use these flags
cflags.release = $(cflags.common) -O3
ldflags.release = $(ldflags.common) 

CC = gcc
CFLAGS := $(cflags.$(BUILD))
LDFLAGS := $(ldflags.$(BUILD))

.PHONY: all
all: c63enc c63dec c63pred

%.o: %.c $(CONFIG)
	$(CC) $< $(CFLAGS) -c -o $@

c63enc: c63enc.o dsp.o tables.o io.o c63_write.o c63.h common.o me.o timer.o
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@
c63dec: c63dec.c dsp.o tables.o io.o c63.h common.o me.o timer.o
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@
c63pred: c63dec.c dsp.o tables.o io.o c63.h common.o me.o timer.o
	$(CC) $^ -DC63_PRED $(CFLAGS) $(LDFLAGS) -o $@

.PHONY: foreman
foreman: c63enc
	#./c63enc -w 352 -h 288 -o $(FILE) -f $(FRAMES) foreman_cif.yuv
	./c63enc -w 352 -h 288 -o $(FILE).c63 -f $(FRAMES) $(FILE).yuv

test.c63: c63enc
	./c63enc -w 352 -h 288 -o test.c63 -f $(FRAMES) foreman_cif.yuv

.PHONY: diff
diff:
	diff $(FILE).c63 test/$(FILE)_10f.c63

test.yuv : $(FILE).c63 c63dec
	-./c63dec $(FILE).c63 test.yuv

.PHONY: mplay-orig mplay
mplay-orig:
	mplayer foreman_cif.yuv -demuxer rawvideo -rawvideo w=352:h=288
mplay: test.yuv
	mplayer test.yuv -demuxer rawvideo -rawvideo w=352:h=288

.PHONY: clean
clean:
	rm -f *.o c63enc c63dec c63pred

$(REPORT_DIR)/$(HOST) :
	mkdir $@
	cat /proc/cpuinfo > $@/$(HOST).txt

$(REPORT_DIR)/$(HOST)/$(TIME).txt : $(REPORT_DIR)/$(HOST)
	echo $(TIME) > $@
	echo >> $@
	grep '\(\w\+\) *\b\1_\|\(\b\w\+\)_[a-Z]* *\2\b' $(CONFIG) >> $@
	echo >> $@
	make foreman >> $@
	echo >> $@
	gprof c63enc >> $@

.PHONY : report
report : $(REPORT_DIR)/$(HOST)/$(TIME).txt
