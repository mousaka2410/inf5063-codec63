#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dsp.h"
#include "me.h"

#include <mmintrin.h> //MMX
#include <xmmintrin.h> //sse
#include <emmintrin.h> //sse2
#include <tmmintrin.h> //ssse3
#include <immintrin.h> //avx/avx2


//#define me_block_8x8    me_block_8x8_empty

//#include <mmintrin.h> //for _mm_prefetch
//for testing
static void me_block_8x8_e(struct c63_common *cm, int mb_x, int mb_y,
			 uint8_t *orig, uint8_t *ref, int color_component) {
  /*
    empty takes 16 to 17 ms, 
   */
}

/*
SSE version 1.0 of me_block_8x8, with sad_block_8x8_sse_1.3V1 inlined. Total time 60ms, measured on the lab machine.
V2: tried to do the load seperatly(into own named XMM register,not into parameters of
sad), lost 6ms. 
BUGS: None
TESTED: yes
############################SECTIONS###############################

               #######Write to stack memory######
Writes memory from uint8_t block1 and block2 to uint64_t dirty_hack1 and dirty_hack2. Memory address to
consenate is calculated stride*x [x =0-7]  
The original data is the samme for all calculations  inside the for loops, therefor no need to load this every time, saves ~20ms. 
Reason for use: 
-Consenate memory for sse load. 
-Can use aligned load operators. 
-Memory performence 
per frame time:  dirty hack1 is outside the loops, so little effect on total run time
                 dirty hack2 28ms measured on the lab machine

                 #######Calculations#######
Do the actual calculations. Uses _mm_sad_epu: abs(x1-x2), and _mm_adds_epu: x1+x2. 
per frame time:  26ms measured on the lab machine

                    ######Writeout#######
writeout sum to uint16_t get_res, only 2 values contains actual data. 
per frame time:  11ms measured on the lab machine		    
*/
static void me_block_8x8_sse(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x,y;

  int mx = mb_x * 8;
  int my = mb_y * 8;
  

  int best_sad = INT_MAX;

  //printf("left right:  %d  %d\n",left,right);
  /*
    antar at left - right kan deles på 8
    hvis ikke må resterende funksjoner kall gjøres under x loop

  */
  uint8_t *block1, *block2;
  uint64_t dirty_hack1[8]  __attribute__ ((aligned (32))), dirty_hack2[8]  __attribute__ ((aligned (32)));
  
  
  __m128i sum;
  //__m128i load1,load2,load3,load4,load5,load6,load7,load8;
  
  uint16_t get_res[16]  __attribute__ ((aligned (32)));//holds the sum after the store operation
  
  
  int stride_r = w;
  //register -5ms
  register int res =0;
  
  /*
     #######Write to stack memory######
     Have little effect on totel time
  */
  
  block1 = orig + my*w+mx;
  dirty_hack1[0] =((uint64_t*)&block1[0])[0]; dirty_hack1[1] =((uint64_t*)&block1[stride_r])[0];
  dirty_hack1[2] =((uint64_t*)&block1[stride_r*2])[0]; dirty_hack1[3] =((uint64_t*)&block1[stride_r*3])[0];
  dirty_hack1[4] =((uint64_t*)&block1[stride_r*4])[0]; dirty_hack1[5] =((uint64_t*)&block1[stride_r*5])[0];
  dirty_hack1[6] =((uint64_t*)&block1[stride_r*6])[0]; dirty_hack1[7] =((uint64_t*)&block1[stride_r*7])[0];
  
  
  for (y = top; y < bottom; ++y)
    {
      //_mm_prefetch(ref + y*w,1); //no effect
      //loop unrolling med 4: 68-69ms med 8: 69ms 4 bedre.. Men sparer bare 1 ms på dette...
      for (x = left; x < right; x++)
	{
	  
	  
      //Start BLOKK1 if we do loop unrolling, but it hase very little payoff
	  res =0;
	  /*
	     #######Write to stack memory######
	     totel time: 21ms
	  */
	  
	  block2 = ref + y*w + x; 
	  //__builtin_prefetch(ref + (y)*w+x,2);
	  dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[stride_r])[0];
	  dirty_hack2[2] =((uint64_t*)&block2[stride_r*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[stride_r*3])[0];
	  dirty_hack2[4] =((uint64_t*)&block2[stride_r*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[stride_r*5])[0];
	  dirty_hack2[6] =((uint64_t*)&block2[stride_r*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[stride_r*7])[0];
	  
	 
	   
	  /*
	         #######Calculations#######
		 total time: 28ms
	  */
	  
	  sum = _mm_sad_epu8( _mm_loadu_si128((__m128i*)&dirty_hack1[0]) , _mm_loadu_si128((__m128i*)&dirty_hack2[0]));
	  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[2]),_mm_loadu_si128((__m128i*)&dirty_hack2[2]))); 
	  sum = _mm_adds_epu16(sum, _mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[4]), _mm_loadu_si128((__m128i*)&dirty_hack2[4])));
	  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[6]) ,_mm_loadu_si128((__m128i*)&dirty_hack2[6])));
	  
	
	   
	  /*
	    writeout
	    total time 8ms
	  */
	  
	  //+8ms
	  _mm_store_si128((__m128i*)&get_res[0],sum);
	  
	  //+2ms
	  res += get_res[0]; 
	  res += get_res[4]; 
	  
	  if (res < best_sad)
	    {
	      mb->mv_x = x - mx;
	      mb->mv_y = y - my;
	      best_sad = res;
	    }
	  
	  //END BLOKK1
	  
	  
	  
	  /*
	  //TEST
	  int sad;
	  
	  sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);
	  if(sad != res) {
	  printf("me_block: %d  %d\n",sad,res);
	  
	  }
	  */
	  
	  /* printf("(%4d,%4d) - %d\n", x, y, sad); */
	  
	}
    }
  
  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */
  
  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */
  
  mb->use_mv = 1;
}


#ifdef __AVX__
static void me_block_8x8_avx(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x,y;

  int mx = mb_x * 8;
  int my = mb_y * 8;
  

  int best_sad = INT_MAX;

  //printf("left right:  %d  %d\n",left,right);
  /*
    antar at left - right kan deles på 8
    hvis ikke må resterende funksjoner kall gjøres under x loop

  */
  uint8_t *block1, *block2;
  uint64_t dirty_hack1[8]  __attribute__ ((aligned (32))), dirty_hack2[8]  __attribute__ ((aligned (32)));
  
  __m256i sum,sum2;
  
    
  uint16_t get_res[16]  __attribute__ ((aligned (32)));


  int stride_r = w;
  //register -5ms
  register int res =0;

  block1 = orig + my*w+mx;

  dirty_hack1[0] =((uint64_t*)&block1[0])[0]; dirty_hack1[1] =((uint64_t*)&block1[stride_r])[0];
  dirty_hack1[2] =((uint64_t*)&block1[stride_r*2])[0]; dirty_hack1[3] =((uint64_t*)&block1[stride_r*3])[0];
  dirty_hack1[4] =((uint64_t*)&block1[stride_r*4])[0]; dirty_hack1[5] =((uint64_t*)&block1[stride_r*5])[0];
  dirty_hack1[6] =((uint64_t*)&block1[stride_r*6])[0]; dirty_hack1[7] =((uint64_t*)&block1[stride_r*7])[0];
  
  
  for (y = top; y < bottom; ++y)
    {
      
      //loop unrolling med 4: 68-69ms med 8: 69ms 4 bedre.. Men sparer bare 1 ms på dette...
      for (x = left; x < right; x+=2)
	{
	  
	  
	  //Start BLOK1
      res =0;
      block2 = ref + y*w + x;
      
      //__builtin_prefetch(ref + (y)*w+x,2);
      dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[stride_r])[0];
      dirty_hack2[2] =((uint64_t*)&block2[stride_r*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[stride_r*3])[0];
      dirty_hack2[4] =((uint64_t*)&block2[stride_r*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[stride_r*5])[0];
      dirty_hack2[6] =((uint64_t*)&block2[stride_r*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[stride_r*7])[0];
      
      sum = _mm256_sad_epu8( _mm256_lddqu_si256((__m256i*)&dirty_hack1[0]),_mm256_lddqu_si256((__m256i*)&dirty_hack2[0]));
      sum = _mm256_adds_epu16(sum,_mm256_sad_epu8( _mm256_lddqu_si256((__m256i*)&dirty_hack1[4]),_mm256_lddqu_si256((__m256i*)&dirty_hack2[4])));
      
      //prøvde denne men, legger til +10 ms 
      //sum = _mm256_adds_epu16(sum,_mm256_unpackhi_epi16(sum,sum));
      
      
      
      /*
	writeout
      */
      
      //+10ms
      _mm256_store_si256((__m256i*)&get_res[0],sum);
      
      
      
      //+5ms
      res += get_res[0]; 
      res += get_res[4]; 
      res += get_res[8]; 
      res += get_res[12];
      
      //*result = res;
      
      if (res < best_sad)
	{
	  mb->mv_x = x - mx;
	  mb->mv_y = y - my;
	  best_sad = res;
	}
      
      //END BLOK1
      
      //Start BLOK2
      res =0;
      block2++;
      
      //__builtin_prefetch(ref + (y)*w+x,2);
      dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[stride_r])[0];
      dirty_hack2[2] =((uint64_t*)&block2[stride_r*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[stride_r*3])[0];
      dirty_hack2[4] =((uint64_t*)&block2[stride_r*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[stride_r*5])[0];
      dirty_hack2[6] =((uint64_t*)&block2[stride_r*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[stride_r*7])[0];
      
      sum = _mm256_sad_epu8( _mm256_lddqu_si256((__m256i*)&dirty_hack1[0]),_mm256_lddqu_si256((__m256i*)&dirty_hack2[0]));
      sum = _mm256_adds_epu16(sum,_mm256_sad_epu8( _mm256_lddqu_si256((__m256i*)&dirty_hack1[4]),_mm256_lddqu_si256((__m256i*)&dirty_hack2[4])));
      
      //prøvde denne med legger til +10 ms 
      //sum = _mm256_adds_epu16(sum,_mm256_unpackhi_epi16(sum,sum));
      
      
      
      
      /*
       writeout
      */

      //+10ms
      _mm256_store_si256((__m256i*)&get_res[0],sum);
      
      //+5ms
      res += get_res[0]; 
      res += get_res[4]; 
      res += get_res[8]; 
      res += get_res[12];
      
      
      
      if (res < best_sad)
	{
	  mb->mv_x = x - mx;
	  mb->mv_y = y - my;
	  best_sad = res;
	}
      
      //END BLOK2
      
      
      
      
      /*
	//TEST
      int sad;
       
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);
      if(sad != res) {
	printf("me_block: %d  %d",sad,res);

      }
      */

      /* printf("(%4d,%4d) - %d\n", x, y, sad); */
      
    }
  }

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}
#endif  /*__AVX__*/ 


/* Motion estimation for 8x8 block */
static void me_block_8x8_old(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;
  

  int best_sad = INT_MAX;

  //printf("left right:  %d  %d\n",left,right);
  //printf("range %d\n",range);
  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      int sad;
      //__builtin_prefetch(ref + (y)*w+x,2);
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);

      /* printf("(%4d,%4d) - %d\n", x, y, sad); */

      if (sad < best_sad)
      {
        mb->mv_x = x - mx;
        mb->mv_y = y - my;
        best_sad = sad;
      }
    }
  }

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}

void c63_motion_estimate(struct c63_common *cm)
{
  /* Compare this frame with previous reconstructed frame */
  int mb_x, mb_y;

  /* Luma */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->Y,
          cm->refframe->recons->Y, Y_COMPONENT);
    }
  }

  /* Chroma */
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->U,
          cm->refframe->recons->U, U_COMPONENT);
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->V,
          cm->refframe->recons->V, V_COMPONENT);
    }
  }
}

/* Motion compensation for 8x8 block */
static void mc_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *predicted, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  if (!mb->use_mv) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
  int right = left + 8;
  int bottom = top + 8;

  int w = cm->padw[color_component];

  /* Copy block from ref mandated by MV */
  int x, y;

  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
    }
  }
}

void c63_motion_compensate(struct c63_common *cm)
{
  int mb_x, mb_y;

  /* Luma */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->Y,
          cm->refframe->recons->Y, Y_COMPONENT);
    }
  }

  /* Chroma */
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->U,
          cm->refframe->recons->U, U_COMPONENT);
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->V,
          cm->refframe->recons->V, V_COMPONENT);
    }
  }
}
