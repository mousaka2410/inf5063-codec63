static void transpose_block_sse_1_0(float *in_data, float *out_data)
{
 
  __m128 row1,row2,row3,row4;
  
 
  //first 4x4
  row1 = _mm_load_ps(&in_data[0]);
  row2 = _mm_load_ps(&in_data[8]);
  row3 = _mm_load_ps(&in_data[16]);
  row4 = _mm_load_ps(&in_data[24]);
  _MM_TRANSPOSE4_PS(row1, row2, row3, row4);
  _mm_store_ps(&out_data[0],row1);
  _mm_store_ps(&out_data[8],row2);
  _mm_store_ps(&out_data[16],row3);
  _mm_store_ps(&out_data[24],row4);

  //third 4x4
   row1 = _mm_load_ps(&in_data[32]);
  row2 = _mm_load_ps(&in_data[40]);
  row3 = _mm_load_ps(&in_data[48]);
  row4 = _mm_load_ps(&in_data[56]);
  _MM_TRANSPOSE4_PS(row1, row2, row3, row4);
  _mm_store_ps(&out_data[4],row1);
  _mm_store_ps(&out_data[12],row2);
  _mm_store_ps(&out_data[20],row3);
  _mm_store_ps(&out_data[28],row4);

  //second 4x4
  row1 = _mm_load_ps(&in_data[4]);
  row2 = _mm_load_ps(&in_data[12]);
  row3 = _mm_load_ps(&in_data[20]);
  row4 = _mm_load_ps(&in_data[28]);
   _MM_TRANSPOSE4_PS(row1, row2, row3, row4);
  _mm_store_ps(&out_data[32],row1);
  _mm_store_ps(&out_data[40],row2);
  _mm_store_ps(&out_data[48],row3);
  _mm_store_ps(&out_data[56],row4);

  
  //fourth 4x4
  row1 = _mm_load_ps(&in_data[36]);
  row2 = _mm_load_ps(&in_data[44]);
  row3 = _mm_load_ps(&in_data[52]);
  row4 = _mm_load_ps(&in_data[60]);
  _MM_TRANSPOSE4_PS(row1, row2, row3, row4);
  _mm_store_ps(&out_data[36],row1);
  _mm_store_ps(&out_data[44],row2);
  _mm_store_ps(&out_data[52],row3);
  _mm_store_ps(&out_data[60],row4);

  /*
  //Test
  int x;
  float test_out[64] __attribute__ ((aligned (16)));
  transpose_block_o(in_data,&test_out[0]);
  for(x =0; x<64;x++) {
    if(test_out[x] != out_data[x]) {
      printf("Bug in Transpose x:%d test_data: %f  out_data: %f \n",x,test_out[x],out_data[x]);
    }
  }
  */
  
}
