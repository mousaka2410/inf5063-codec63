#ifndef C63_DSP_H_
#define C63_DSP_H_

#define ISQRT2 0.70710678118654f

#include <inttypes.h>

#ifdef CONFIG_FILE
#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)
#include STRINGIZE_VALUE_OF(CONFIG_FILE)
#else
#include "config.h"
#endif /* CONFIG_FILE */

void dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl);

void dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl);

void sad_block_8x8(uint8_t *block1, uint8_t *block2, int stride, int *result);

#endif  /* C63_DSP_H_ */
