#ifndef TIMER_H
#define TIMER_H

#if defined(MONITOR_TIME) &&  MONITOR_TIME==1
struct TimerStruct{
    unsigned long long effectiveTime;
    unsigned long long callTime;
    unsigned long long functionStart;
};

#define FUNCTION_TO_TIME(x) extern struct TimerStruct x##_time;
#include "functions_to_time.x"
#undef FUNCTION_TO_TIME

/* Must be called once at start of program to monitor */
void init_timer(void);

/* Prints the times of the functions to monitor. Must not be called
 * inside any of the functions it monitors */
void print_times(void);

/* Should not be used directly, use macros below instead */
__inline__ void start_of_call(struct TimerStruct *ts);
__inline__ void end_of_call(void);

/* Put this at the start of a function to monitor. Must be a function
 * that is specified in functions_to_time.x */
#define START_OF_CALL(x) start_of_call(&x##_time)

/* Put this by the end of a function to monitor, just before the function
 * returns . Must be a function that is specified in functions_to_time.x,
 * and should be a function that has a START_OF_CALL at the beginning. */
#define END_OF_CALL() end_of_call()

#else
#define START_OF_CALL(x)
#define END_OF_CALL()
#define init_timer()
#define print_times()
#endif /* MONITOR_TIME */

#endif /* TIMER_H */
